#ifndef __AW_PRIVATE_H__
#define __AW_PRIVATE_H__

GdkRGBA aw_value_to_rgba (const GValue *value);
void aw_rgba_to_value (GdkRGBA rgba, GValue *value);

#endif
