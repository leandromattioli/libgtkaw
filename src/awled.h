/***************************************************************************
 *            awled.h
 *
 *  Dom janeiro 03 12:10:44 2016
 *  Copyright  2016  Leandro Resende Mattioli
 *  <leandro.mattioli@gmail.com>
 ****************************************************************************/
/*
 * awled.h
 *
 * Copyright (C) 2016 - Leandro Resende Mattioli
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __AW_LED_H__
#define __AW_LED_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define AW_TYPE_LED             (aw_led_get_type ())
#define AW_LED(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), AW_TYPE_LED, AwLed))
#define AW_LED_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), AW_TYPE_LED, AwLedClass))
#define AW_IS_LED(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AW_TYPE_LED))
#define AW_IS_LED_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), AW_TYPE_LED))
#define AW_LED_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), AW_TYPE_LED, AwLedClass))

typedef struct _AwLed        AwLed;
typedef struct _AwLedClass   AwLedClass;
typedef struct _AwLedPrivate AwLedPrivate;

typedef enum {
    AW_SHAPE_OVAL,
    AW_SHAPE_RECTANGULAR
} AwLedShape;

struct _AwLed {
    GtkWidget  parent_instance;
    
    /*< private >*/
    AwLedPrivate* priv;
};

struct _AwLedClass {
    GtkWidgetClass parent_class;
};

GDK_AVAILABLE_IN_ALL
GType aw_led_get_type (void) G_GNUC_CONST;

GtkWidget* aw_led_new ();

//Accessors
gboolean aw_led_get_active (AwLed* led);
void aw_led_set_active (AwLed* led, gboolean active);
AwLedShape aw_led_get_shape(AwLed* led);
void aw_led_set_shape(AwLed* led, AwLedShape shape);
GdkRGBA aw_led_get_color_on(AwLed* led);
void aw_led_set_color_on(AwLed* led, GdkRGBA color_on);
GdkRGBA aw_led_get_color_off(AwLed* led);
void aw_led_set_color_off(AwLed* led, GdkRGBA color_off);

G_END_DECLS

#endif /* __AW_LED_H__ */
