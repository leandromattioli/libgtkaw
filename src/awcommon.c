#include <stdio.h>
#include <gtk/gtk.h>
#include "gtkaw.h"
#include "awprivate.h"

void 
aw_init() 
{
    static int dummy = 0;
    dummy++;
}


GdkRGBA
aw_value_to_rgba (const GValue *value)
{
  GdkColor *color;
  GdkRGBA rgba;

  color = g_value_get_boxed (value);

  rgba.red = color->red / 65535.0;
  rgba.green = color->green / 65535.0;
  rgba.blue = color->blue / 65535.0;
  rgba.alpha = 1.0;

  return rgba;
}


void
aw_rgba_to_value (GdkRGBA rgba, GValue *value)
{
  GdkColor color;

  color.red = (guint16) (rgba.red * 65535 + 0.5);
  color.green = (guint16) (rgba.green * 65535 + 0.5);
  color.blue = (guint16) (rgba.blue * 65535 + 0.5);

  g_value_set_boxed (value, &color);
}

