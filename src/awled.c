/*
 * awled.c
 *
 * Copyright (C) 2016 - Leandro Resende Mattioli
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
 //@TODO glade icon, complete documentation
 //@FIXME Register Shape enum with GLib
 
/**
 * SECTION:awled
 * @short_description: Simple round or rectangular LED
 * @title: AwLed
 * @section_id:
 * @stability: Stable
 * @include: awled.h
 * @image: screenshot-led.png
 *
 * Simple digital LED with colors associated to its 2 actives (on, off)
 */

#include <gtk/gtk.h>
#include "awled.h"
#include "awprivate.h"

struct _AwLedPrivate {
  GdkRGBA    color_on;
  GdkRGBA    color_off;
  AwLedShape shape;
  gboolean   active;
  GdkWindow  *input_window;
};

enum {
  PROP_0,
  PROP_ACTIVE,
  PROP_COLOR_ON,
  PROP_COLOR_OFF,
  LAST_PROPERTY,
  PROP_SHAPE
};

static GParamSpec *properties[LAST_PROPERTY] = { NULL, };


// ===========================================================================
// Private methods prototypes
// ===========================================================================

static void      aw_led_set_property          (GObject      *object,
                                               guint         prop_id,
                                               const GValue *value,
                                               GParamSpec   *pspec);
static void      aw_led_get_property          (GObject    *object,
                                               guint       prop_id, 
                                               GValue     *value,
                                               GParamSpec *pspec);
static void      aw_led_realize               (GtkWidget      *widget);
static void      aw_led_unrealize             (GtkWidget      *widget);
static void      aw_led_map                   (GtkWidget      *widget);
static void      aw_led_unmap                 (GtkWidget      *widget);
static void      aw_led_size_allocate         (GtkWidget      *widget,
                                               GtkAllocation  *allocation);
static void      aw_led_get_preferred_width (GtkWidget *widget,
                                              gint      *minimal_width,
                                              gint      *natural_width);
static void      aw_led_get_preferred_height (GtkWidget *widget,
                                              gint      *minimal_height,
                                              gint      *natural_height);
static gboolean  aw_led_draw                (GtkWidget      *widget,
                                             cairo_t *cr);
                                             
// ===========================================================================
// Private methods implementation
// ===========================================================================

G_DEFINE_TYPE_WITH_PRIVATE (AwLed, aw_led, GTK_TYPE_WIDGET)

static void
aw_led_class_init (AwLedClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property         = aw_led_set_property;
  object_class->get_property         = aw_led_get_property;

  widget_class->realize              = aw_led_realize;
  widget_class->unrealize            = aw_led_unrealize;
  widget_class->map                  = aw_led_map;
  widget_class->unmap                = aw_led_unmap;
  widget_class->size_allocate        = aw_led_size_allocate;
  widget_class->get_preferred_width  = aw_led_get_preferred_width;
  widget_class->get_preferred_height = aw_led_get_preferred_height;
  widget_class->draw                 = aw_led_draw;

  properties[PROP_ACTIVE] = g_param_spec_boolean ("active", "Active", 
                                                  "LED State",
                                                  FALSE, G_PARAM_READWRITE);
  properties[PROP_COLOR_ON] = g_param_spec_boxed ("color_on", "Color ON",
                                                  "Color for the ON state",
                                                   GDK_TYPE_RGBA,
                                                   G_PARAM_READWRITE);
  properties[PROP_COLOR_OFF] = g_param_spec_boxed ("color_off", "Color OFF",
                                                   "Color for the OFF state",
                                                   GDK_TYPE_RGBA,
                                                   G_PARAM_READWRITE);
  /*properties[PROP_SHAPE] = g_param_spec_enum ("shape", "LED Shape", 
                                              "Oval or rectangular",
                                              AW_TYPE_SHAPE, AW_SHAPE_OVAL,
                                              G_PARAM_READWRITE);*/
  
  g_object_class_install_properties (object_class, LAST_PROPERTY, properties);
                                   
}


static void
aw_led_init (AwLed *led)
{
  gtk_widget_set_has_window (GTK_WIDGET (led), FALSE);

  led->priv = aw_led_get_instance_private (led);

  led->priv->active          = FALSE;
  led->priv->color_on.red    = 0.0;
  led->priv->color_on.green  = 0.6;
  led->priv->color_on.blue   = 0.0;
  led->priv->color_on.alpha  = 1.0;
  led->priv->color_off.red   = 0.6;
  led->priv->color_off.green = 0.0;
  led->priv->color_off.blue  = 0.0;
  led->priv->color_off.alpha = 1.0;
}


static void
aw_led_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  AwLed *led = AW_LED (object);

  switch (prop_id)
  {
    case PROP_ACTIVE:
      aw_led_set_active(led, g_value_get_boolean (value));
      break;
    case PROP_SHAPE:
      aw_led_set_shape(led, g_value_get_enum(value));
      break;
    case PROP_COLOR_ON:
      aw_led_set_color_on(led, aw_value_to_rgba(value));
      break;
    case PROP_COLOR_OFF:
      aw_led_set_color_off(led, aw_value_to_rgba(value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  gtk_widget_queue_draw( GTK_WIDGET(object) );
}


static void
aw_led_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
 AwLed *led = AW_LED (object);

  switch (prop_id)
    {
    case PROP_ACTIVE:
      g_value_set_boolean (value, led->priv->active);
      break;
    case PROP_SHAPE:
      g_value_set_enum(value, led->priv->shape);
      break;
    case PROP_COLOR_ON:
      aw_rgba_to_value(led->priv->color_on, value);
      break;
    case PROP_COLOR_OFF:
      aw_rgba_to_value(led->priv->color_off, value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
aw_led_realize (GtkWidget *widget)
{
  AwLed         *led = AW_LED (widget);
  GtkAllocation allocation;
  GdkWindowAttr attributes;
  gint          attributes_mask;

  GTK_WIDGET_CLASS (aw_led_parent_class)->realize (widget);

  gtk_widget_get_allocation (widget, &allocation);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x           = allocation.x;
  attributes.y           = allocation.y;
  attributes.width       = allocation.width;
  attributes.height      = allocation.height;
  attributes.wclass      = GDK_INPUT_ONLY;
  attributes.event_mask  = (gtk_widget_get_events (widget) | 
                            GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK |
                            GDK_BUTTON_RELEASE_MASK);

  attributes_mask = GDK_WA_X | GDK_WA_Y;

  led->priv->input_window = gdk_window_new (gtk_widget_get_window (widget),
                                            &attributes, attributes_mask);
  gdk_window_set_user_data (led->priv->input_window, led);  
}


static void
aw_led_unrealize (GtkWidget *widget)
{
  AwLed *led = AW_LED (widget);

  if (led->priv->input_window) {
      gdk_window_destroy (led->priv->input_window);
      led->priv->input_window = NULL;
  }

  GTK_WIDGET_CLASS (aw_led_parent_class)->unrealize (widget);
}


static void
aw_led_map (GtkWidget *widget)
{
  AwLed *led = AW_LED (widget);

  GTK_WIDGET_CLASS (aw_led_parent_class)->map (widget);

  if (led->priv->input_window)
    gdk_window_show (led->priv->input_window);

}


static void
aw_led_unmap (GtkWidget *widget)
{
  AwLed *led = AW_LED (widget);
  if (led->priv->input_window)
    gdk_window_hide (led->priv->input_window);

  GTK_WIDGET_CLASS (aw_led_parent_class)->unmap (widget);
}


static void
aw_led_size_allocate (GtkWidget     *widget,
                      GtkAllocation *allocation)
{
  AwLed *led = AW_LED (widget);

  gtk_widget_set_allocation (widget, allocation);

  if (gtk_widget_get_realized (widget)) {
      gdk_window_move_resize (led->priv->input_window,
                              allocation->x, allocation->y,
                              allocation->width, allocation->height);
  }
}


static void
aw_led_get_preferred_width (GtkWidget *widget,
                            gint      *minimal_width,
                            gint      *natural_width)
{
  *minimal_width = 20;
  *natural_width = 35;
}


static void
aw_led_get_preferred_height (GtkWidget *widget,
                             gint      *minimal_height,
                             gint      *natural_height)
{
  *minimal_height = 20;
  *natural_height = 35;
}


static gboolean
aw_led_draw (GtkWidget *widget,
             cairo_t   *cr)
{
  gint width, height;
  AwLed* led = AW_LED(widget);

  width = gtk_widget_get_allocated_width (widget);
  height = gtk_widget_get_allocated_height (widget);
  
  AwLedPrivate* priv = led->priv;
  
  GdkRGBA color = (priv->active) ? priv->color_on : priv->color_off;

  cairo_arc (cr, width / 2.0, height / 2.0,
             MIN (width, height) / 2.0, 0, 2 * G_PI);

  gdk_cairo_set_source_rgba (cr, &color);

  cairo_fill (cr);
  
  return TRUE;
}


// ===========================================================================
// Public API
// ===========================================================================

GtkWidget *
aw_led_new () {
  return g_object_new (AW_TYPE_LED, "active", FALSE, NULL);
}


// ===========================================================================
// Accessors
// ===========================================================================


gboolean aw_led_get_active (AwLed* led) {
    return led->priv->active;
}


void aw_led_set_active (AwLed* led, gboolean active) {
    led->priv->active = active;
    gtk_widget_queue_draw (GTK_WIDGET(led));
}


AwLedShape aw_led_get_shape(AwLed* led) {
    return led->priv->shape;
}


void aw_led_set_shape(AwLed* led, AwLedShape shape) {
    led->priv->shape = shape;
    gtk_widget_queue_draw (GTK_WIDGET(led));
}


GdkRGBA aw_led_get_color_on(AwLed* led) {
    return led->priv->color_on;
}


void aw_led_set_color_on(AwLed* led, GdkRGBA color_on) {
    led->priv->color_on = color_on;
    gtk_widget_queue_draw (GTK_WIDGET(led));
}


GdkRGBA aw_led_get_color_off(AwLed* led) {
    return led->priv->color_off;
}


void aw_led_set_color_off(AwLed* led, GdkRGBA color_off) {
    led->priv->color_off = color_off;
    gtk_widget_queue_draw (GTK_WIDGET(led));
}
