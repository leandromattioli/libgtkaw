#include <gtk/gtk.h>
#include <gtkaw.h>

void led_clicked_cb(GtkWidget *widget) {
    AwLed *led = AW_LED(widget);
    aw_led_set_active(led, !aw_led_get_active(led));
}

int main(int argc, char* argv[]) {
    GtkBuilder* builder;
    GtkWidget *window, *led, *box;
    
    gtk_init(&argc, &argv);
    aw_init();
    
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "interface.ui", NULL);
    
    window = (GtkWidget*) gtk_builder_get_object(builder, "window1");
    led = (GtkWidget*) gtk_builder_get_object(builder, "led1");
    box = (GtkWidget*) gtk_builder_get_object(builder, "box1");
    
    gtk_widget_show_all(window);
 
    g_signal_connect(led, "button-press-event", G_CALLBACK(led_clicked_cb),
                     NULL);   
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);    
    
    gtk_main();
}
