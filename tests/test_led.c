#include <gtk/gtk.h>
#include <awled.h>

int main(int argc, char* argv[]) {
    GtkWidget *window, *led;
    gtk_init(&argc, &argv);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    led = aw_led_new();
    //aw_led_set_state(AW_LED(led), TRUE);
    gtk_container_add(GTK_CONTAINER(window), led);
    gtk_widget_show_all(window);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_main();
}
